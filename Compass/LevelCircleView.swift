//
//  LevelCircleView.swift
//  Compass
//
//  Created by Денис Матвеев on 11/12/2019.
//  Copyright © 2019 Денис Матвеев. All rights reserved.
//

import UIKit

@IBDesignable class LevelCircleView: UIView {
    
    var crosshairSizeFactor: CGFloat = 0.23
    var levelCircleSizeFactor: CGFloat = 0.55
    var crosshairColor: UIColor = .label
    
    // For circle using the custom color from assets
    @IBInspectable var circleColor: UIColor = .systemGray3
    var crosshairSize: CGFloat {
        get {
            return bounds.width * crosshairSizeFactor
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        // Drawing background
        let circlePath = UIBezierPath.init(ovalIn: bounds)
        //let circleColor = UIColor(white: 1, alpha: 0.11)
        circleColor.setFill()
        circlePath.fill()
        
        // Drawing the crosshair
        let crosshairPath = UIBezierPath()
        let leftPoint = CGPoint(
            x: bounds.midX - crosshairSize / 2, y: bounds.midY)
        let rightPoint = CGPoint(
            x: bounds.midX + crosshairSize / 2, y: bounds.midY)
        let topPoint = CGPoint(
            x: bounds.midX, y: bounds.midY - crosshairSize / 2)
        let bottomPoint = CGPoint(
            x: bounds.midX, y: bounds.midY + crosshairSize / 2)
        crosshairPath.move(to: leftPoint)
        crosshairPath.addLine(to: rightPoint)
        crosshairPath.move(to: topPoint)
        crosshairPath.addLine(to: bottomPoint)
            
        //let crosshairColor = UIColor(white: 1, alpha: 0.9)
        crosshairColor.setStroke()
            
        crosshairPath.stroke()
        
    }

}
