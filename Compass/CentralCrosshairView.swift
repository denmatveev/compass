//
//  CentralCrosshairView.swift
//  Compass
//
//  Created by Денис Матвеев on 06/12/2019.
//  Copyright © 2019 Денис Матвеев. All rights reserved.
//

import UIKit

@IBDesignable class CentralCrosshairView: UIView {
    
    private struct Constants {
        static let crosshairSizeFactor: CGFloat = 0.45
        static let crosshairLineWidth: CGFloat = 1
        static let crosshairColor: UIColor = .label
        static let pointerLength: CGFloat = 50
        static let pointerWidth: CGFloat = 4
        static let levelCircleSizeFactor: CGFloat = 0.55
        static let levelCrosshairSizeFactor: CGFloat = 0.23
    }
    
    var crosshairSize: CGFloat {
        get {
            return bounds.width * Constants.crosshairSizeFactor
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        // Defining the center of the view rectangle
        let center = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
        
        // Drawing crosshair
        // Create the path
        let linePath = UIBezierPath()
        linePath.lineWidth = Constants.crosshairLineWidth
        
        drawCrosshair(path: linePath, center: center, fullWidth: crosshairSize)
        
        Constants.crosshairColor.setStroke()
        linePath.stroke()
        
        // Drawing central division
        let divisionPath = UIBezierPath()
        divisionPath.lineWidth = Constants.pointerWidth
        
        divisionPath.move(to: CGPoint(
            x: center.x,
            y: center.y - bounds.height / 2))
        divisionPath.addLine(to: CGPoint(
            x: divisionPath.currentPoint.x,
            y: divisionPath.currentPoint.y + Constants.pointerLength))
        Constants.crosshairColor.setStroke()
        divisionPath.stroke()
    }
    
    func drawCrosshair(path: UIBezierPath, center: CGPoint, fullWidth: CGFloat) {
        // Draws a crosshair on the given path
        let leftPoint = CGPoint(
            x: center.x - fullWidth / 2, y: center.y)
        let rightPoint = CGPoint(
            x: center.x + fullWidth / 2, y: center.y)
        let topPoint = CGPoint(
            x: center.x, y: center.y - fullWidth / 2)
        let bottomPoint = CGPoint(
            x: center.x, y: center.y + fullWidth / 2)
        path.move(to: leftPoint)
        path.addLine(to: rightPoint)
        path.move(to: topPoint)
        path.addLine(to: bottomPoint)
    }
    
}
