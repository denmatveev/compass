//
//  ViewController.swift
//  Compass
//
//  Created by Денис Матвеев on 04/12/2019.
//  Copyright © 2019 Денис Матвеев. All rights reserved.
//

import UIKit
import CoreLocation
import CoreMotion

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var directionLabel: UILabel!
    @IBOutlet weak var coordinatesLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var altitudeLabel: UILabel!
    @IBOutlet weak var compassView: CompassView!
    @IBOutlet weak var centralCrosshairView: CentralCrosshairView!
    @IBOutlet weak var levelCircleCrosshair: LevelCircleView!

    
    let locationManager = CLLocationManager()
    let motionManager = CMMotionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Creating a monospaced font for the direction label
        let monoFont = UIFont.monospacedDigitSystemFont(ofSize: 74, weight: UIFont.Weight.thin)
        headingLabel.font = monoFont
        
        // Centering level circle and setting the width relative to the central crosshair
        let relativeWidth = centralCrosshairView.crosshairSize * levelCircleCrosshair.levelCircleSizeFactor
        levelCircleCrosshair.frame = CGRect(x: 0, y: 0, width: relativeWidth, height: relativeWidth)
        levelCircleCrosshair.center = centralCrosshairView.center
    }
    
    @IBAction func compassTapAction(_ sender: UITapGestureRecognizer) {
        if let heading = locationManager.heading {
            compassView.fixDirection(directionInDeg: Int(heading.trueHeading))
        } else {
            print("Error! Couldn't receive heading data")
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        
        // LOCATION
        // Checking if device supports heading
        guard CLLocationManager.headingAvailable() else {
            return
        }
        locationManager.delegate = self
        
        // Set the minimum distance the phone needs to move before an update event is triggered (for example:  100 meters)
        locationManager.distanceFilter = 100
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer

        // Asking the user for permission
        locationManager.requestWhenInUseAuthorization()
        
        // Start collecting location information
        locationManager.startMonitoringSignificantLocationChanges()
        
        // HEADING
        // Setting heading accuracy in degrees (1º by default)
        locationManager.headingFilter = 1.0
        
        // Starting delivery
        locationManager.startUpdatingHeading()
        
        // MOTION
        guard motionManager.isDeviceMotionAvailable else {
            return
        }
        
        motionManager.deviceMotionUpdateInterval = 0.1
        
        motionManager.startDeviceMotionUpdates(to: .main) {
            [weak self] (data, error) in

            guard let data = data, error == nil else {
                return
            }

            // Counting available radius of movement for the circle
            let crossSize = self?.centralCrosshairView.crosshairSize
            let circleSize = self?.levelCircleCrosshair.bounds.width
            let radius = (crossSize! - circleSize!) / 2
            // x - axis along device -1 0 1
            // y - axis perpendicular to device -1 0 1
            let x = CGFloat(data.gravity.x) * radius
            let y = CGFloat(data.gravity.y) * radius
            
            // Transforming and animating small crosshair
            UIView.animate(withDuration: 0.1) {
                self?.levelCircleCrosshair?.transform = CGAffineTransform(translationX: -x, y: y)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        // Receiving heading updates
        let heading = Int(newHeading.trueHeading)
        
        let direction = getHeadingDirectionString(heading: heading)
        headingLabel.text = "\(String(heading))º"
        directionLabel.text = direction
        
        // Rotating compass
        compassView.rotate(angleInDeg: CGFloat(newHeading.trueHeading))
        
        // Checking if we should play haptics
        if heading == 0 {
            let feedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
            feedbackGenerator.prepare()
            feedbackGenerator.impactOccurred()
        } else if heading % 30 == 0 {
            let feedbackGenerator = UIImpactFeedbackGenerator(style: .light)
            feedbackGenerator.prepare()
            feedbackGenerator.impactOccurred()
        }

    
    }
    
    func getHeadingDirectionString(heading: Int) -> String {
        var directionString: String
        
        switch heading {
        case 337..., -1..<23:
            directionString = "N"
        case 0..<68:
            directionString = "NE"
        case 67..<113:
            directionString = "E"
        case 112..<159:
            directionString = "SE"
        case 158..<203:
            directionString = "S"
        case 202..<248:
            directionString = "SW"
        case 247..<293:
            directionString = "W"
        case 292..<338:
            directionString = "NW"
        default:
            directionString = ""
        }
        return directionString
    }
    
    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
        // Detecting coordinates
        let coordinates = visit.coordinate.latitude
        coordinatesLabel.text = String(coordinates)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // Detecting location and altitude
        if let lastLocation = locationManager.location {
            let altitude = Int(lastLocation.altitude.rounded())
            let geocoder = CLGeocoder()
            // Look up the location and pass it to the completion handler
            geocoder.reverseGeocodeLocation(lastLocation,
                        completionHandler: { (placemarks, error) in
                if error == nil {
                    if let firstLocation = placemarks?.first {
                        self.locationLabel.text = "\(firstLocation.locality!), \(firstLocation.administrativeArea!)"
                    }
                }
                else {
                    // An error occurred during geocoding.
                    self.locationLabel.text = "Location unavailable"
                }
            })
            altitudeLabel.text = "\(String(altitude))m Elevation"
            coordinatesLabel.text = coordinateString(lastLocation.coordinate.latitude, lastLocation.coordinate.longitude)
        }
    }

    func coordinateString(_ latitude: Double,_ longitude: Double) -> String {
        var latSeconds = Int(latitude * 3600)
        let latDegrees = latSeconds / 3600
        latSeconds = abs(latSeconds % 3600)
        let latMinutes = latSeconds / 60
        latSeconds %= 60
        var longSeconds = Int(longitude * 3600)
        let longDegrees = longSeconds / 3600
        longSeconds = abs(longSeconds % 3600)
        let longMinutes = longSeconds / 60
        longSeconds %= 60
        return String(format:"%d°%d′%d″ %@  %d°%d′%d″ %@",
                      abs(latDegrees),
                      latMinutes,
                      latSeconds, latDegrees >= 0 ? "N" : "S",
                      abs(longDegrees),
                      longMinutes,
                      longSeconds,
                      longDegrees >= 0 ? "E" : "W" )
    }
    
    func locationManager(_ manager: CLLocationManager,  didFailWithError error: Error) {
//        if error.code == .denied {
//          // Location updates are not authorized.
//          locationManager.stopMonitoringVisits()
//          return
//        }
        
        // Notify the user of any errors.
        print("Error with location update!")
    }

}

