//
//  CompassView.swift
//  Compass
//
//  Created by Денис Матвеев on 05/12/2019.
//  Copyright © 2019 Денис Матвеев. All rights reserved.
//

import UIKit
import CoreText

@IBDesignable class CompassView: UIView {

    private struct Constants {
        static let compassSizeRatio: CGFloat = 0.8
        static let subDivisionLength: CGFloat = 18
        static let subDivisionWidth: CGFloat = 1
        static let divisionLength: CGFloat = 18
        static let divisionWidth: CGFloat = 3
        static let directionTextPadding: CGFloat = 10
        static let northTriangleWidth: CGFloat = 12
        static let northTriangleBottomOffset: CGFloat = 6
        static let animationDuration: Double = 0.5
        static let fixedArcWidthRatio: CGFloat = 0.25
    }
    
    // Actual radius with scale ratio
    var compassRadius: CGFloat {
        get {
            return bounds.width / 2 * Constants.compassSizeRatio
        }
    }
    var compassCenter: CGPoint {
        get {
            return CGPoint(x: bounds.width / 2, y: bounds.height / 2)
        }
    }
        
    // These params will appear in the inspector
    //@IBInspectable var divisionNumber: Int = 12
    var divisionNumber: Int = 12
    var subDivisionsPerDivision: Int = 14
    var divisionColor: UIColor = .label
    var textColor: UIColor = .label
    var textDirectionsSize: CGFloat = 24
    var textDivisionsSize: CGFloat = 14
    var fixedDirectionTextSize: CGFloat = 20
    
    // Objects for fixing a direction
    private var fixedDirectionText: UILabel!
    private var fixedDegrees: Int = -1
    private var fixedPointerPath = UIBezierPath()
    private var fixedArcRenderer = ArcRenderer()
    
    // Array of views, that are not supposed to rotate along with the main view
    private var excludedFromRotation = [UIView]()
        
    override func draw(_ rect: CGRect) {
        
        // DRAW GRAPHICS
        
        // Drawing compass dividions
        let divisionPath = drawAngularArray(
            divisionNumber,
            Constants.divisionLength,
            Constants.divisionWidth)
        
        let subdivisionPath = drawAngularArray(
            subDivisionsPerDivision * divisionNumber,
            Constants.subDivisionLength,
            Constants.subDivisionWidth)
        
        // Set the stroke color
        divisionColor.setStroke()
        // Draw the stroke
        divisionPath.stroke()
        subdivisionPath.stroke()
        
        // Draw the north triangle pointer
        let trianglePath = UIBezierPath()
        let topPoint = getPolarCoordinates(compassCenter, -.pi / 2, compassRadius + Constants.northTriangleBottomOffset + Constants.northTriangleWidth)
        let leftPoint = CGPoint(
            x: topPoint.x - Constants.northTriangleWidth / 2,
            y: topPoint.y + Constants.northTriangleWidth)
        let rightPoint = CGPoint(
            x: topPoint.x + Constants.northTriangleWidth / 2,
            y: topPoint.y + Constants.northTriangleWidth)
        trianglePath.move(to: topPoint)
        trianglePath.addLine(to: leftPoint)
        trianglePath.addLine(to: rightPoint)
        UIColor.systemRed.setFill()
        trianglePath.fill()
        
        // DRAW TEXT
        
        // Drawing direction text
        let directionChars = ["E", "S", "W", "N"]
        for (index, direction) in directionChars.enumerated() {
            let label = drawAngularText(
            angle: .pi / 2 * CGFloat(index),
            radius: compassRadius - (Constants.divisionLength + Constants.directionTextPadding + textDirectionsSize / 2),
            text: direction,
            font: UIFont.systemFont(ofSize: textDirectionsSize, weight: UIFont.Weight.regular))
            // Adding label to exception list, so they won't rotate with the compass
            excludedFromRotation.append(label)
        }
        
        // Draw division text
        let angleShift = 2 * .pi / CGFloat(divisionNumber)
        for index in 0..<divisionNumber {
            let curAngle = angleShift * CGFloat(index)
            let label = drawAngularText(
                angle: curAngle - .pi / 2,
                radius: bounds.width / 2,
                text: String(360 / divisionNumber * index),
                font: UIFont.systemFont(ofSize: textDivisionsSize, weight: UIFont.Weight.light))
            excludedFromRotation.append(label)
        }
        
        // DRAW FIXED
        
        if fixedDegrees >= 0 {
            let directionInRad = CGFloat(fixedDegrees) * .pi / 180
            
            // Drawing pointer
            let startPoint = getPolarCoordinates(
                compassCenter,
                directionInRad - .pi / 2,
                compassRadius - Constants.divisionLength)
            let endPoint = getPolarCoordinates(
                compassCenter,
                directionInRad - .pi / 2,
                bounds.height / 2)
            fixedPointerPath.lineWidth = Constants.divisionWidth
            divisionColor.setStroke()
            drawDivision(path: fixedPointerPath, startPoint, endPoint)
            fixedPointerPath.stroke()
        }
        
        // Exclude other views from rotating
        for view in excludedFromRotation {
            view.transform = self.transform.inverted()
        }
        
    }
    
     func commonInit() {
        // Setting up arc layer at the start
        fixedArcRenderer.updateBounds(bounds)
        fixedArcRenderer.color = UIColor.systemRed
        layer.addSublayer(fixedArcRenderer.arcLayer)
    }
    
    func fixDirection(directionInDeg: Int) {
        // Call this function to fix a direction
        let directionInRad = CGFloat(directionInDeg) * .pi / 180
            
        if fixedDegrees < 0 {
            fixedDegrees = directionInDeg
            
            // Drawing heading text
            fixedDirectionText = drawAngularText(
                angle: directionInRad - .pi / 2,
                radius: bounds.width / 2 +  fixedDirectionTextSize,
                text: String(directionInDeg),
                font: UIFont.systemFont(
                    ofSize: fixedDirectionTextSize,
                    weight: UIFont.Weight.medium))
            fixedDirectionText.textColor = .label
            fixedDirectionText.transform = self.transform.inverted()
            self.addSubview(fixedDirectionText)
            excludedFromRotation.append(fixedDirectionText)
            
            // Drawing the arc
            commonInit()
            let arcWidth = compassRadius * Constants.fixedArcWidthRatio
            fixedArcRenderer.arcRadius = compassRadius - Constants.divisionLength - arcWidth / 2
            fixedArcRenderer.lineWidth = arcWidth
            fixedArcRenderer.startAngle = CGFloat(fixedDegrees - 90) * .pi / 180
            fixedArcRenderer.endAngle = directionInRad - .pi / 2
        } else {
            fixedDegrees = -1
            fixedDirectionText.removeFromSuperview()
            fixedPointerPath.removeAllPoints()
            fixedArcRenderer.arcLayer.removeFromSuperlayer()
            //fixedArcPath.removeAllPoints()
        }
        self.setNeedsDisplay()
    }
    
    func rotate(angleInRad: CGFloat) {
        // Rotate the compass
        UIView.animate(
        withDuration: Constants.animationDuration,
        delay: 0,
        options: AnimationOptions.allowUserInteraction,
        animations: {
            self.transform = CGAffineTransform(rotationAngle: -angleInRad) // rotate the picture
        },
        completion: nil)
        
        if fixedDegrees >= 0 {
            fixedArcRenderer.endAngle = angleInRad - .pi / 2
        }
        
        // Exclude other views from rotating
        for view in excludedFromRotation {
            view.transform = self.transform.inverted()
        }
        //self.setNeedsDisplay()
    }
    
    func rotate(angleInDeg: CGFloat) {
        let angleInRad =
            angleInDeg * .pi / 180 // convert from degrees to radians
        rotate(angleInRad: angleInRad)
    }
    
    func drawAngularArray(_ number: Int,_ lineLength: CGFloat,_ lineWidth: CGFloat) -> UIBezierPath {
        
        // Create the path
        let linePath = UIBezierPath()

        // Set the path's line width to the height of the stroke
        linePath.lineWidth = lineWidth
        
        // Draw the lines
        let angleShift = 2 * .pi / CGFloat(number)
        for index in 0..<number {
            let startPoint = getPolarCoordinates(
                compassCenter,
                angleShift * CGFloat(index),
                compassRadius - lineLength)
            let endPoint = getPolarCoordinates(
                compassCenter,
                angleShift * CGFloat(index),
                compassRadius)
            
            drawDivision(path: linePath, startPoint, endPoint)
        }
        
        return linePath
    }
    
    func drawDivision(path: UIBezierPath,_ startPoint: CGPoint,_ endPoint: CGPoint) {
        
        // Draws a division on the given path
        // Move the initial point of the path
        path.move(to: startPoint)
        
        // Add a point to the path at the end of the stroke
        path.addLine(to: endPoint)
    }
    
    func drawAngularText(angle: CGFloat, radius: CGFloat,  text: String, font: UIFont) -> UILabel {
        // Defining position using polar coords and a text size shift
        let charShift = CGFloat(text.count)
        let labelPosition = getPolarCoordinates(
        compassCenter.applying(CGAffineTransform(
            translationX: -textDirectionsSize / 2 * charShift,
            y: -textDirectionsSize / 2)),
        angle,
        radius)
        return drawText(origin: labelPosition, text: text, font: font)
        
    }
    
    func drawText(origin: CGPoint,  text: String, font: UIFont) -> UILabel {
        
        // Creates a label at the designated origin and returns its instance
        // Calculate number of chars to define the width of the text frame
        let charCount = text.count
        
        //Create a label within designated frame
        let label = UILabel(frame: CGRect(
            x: origin.x,
            y: origin.y,
            width: textDirectionsSize * CGFloat(charCount),
            height: textDirectionsSize))
        
        // Define text properties
        label.text = text
        label.textColor = textColor
        label.textAlignment = .center
        label.font = font
        
        // Add label as subview and return the pointer
        self.addSubview(label)
        return label
    }
    
    func getPolarCoordinates(_ center: CGPoint,_ angle: CGFloat,_ radius: CGFloat) -> CGPoint {
        return CGPoint(
            x: center.x + radius * cos(angle),
            y: center.y + radius * sin(angle))
    }
    
    private class ArcRenderer {
        var color: UIColor = .systemRed {
          didSet {
            arcLayer.strokeColor = color.cgColor
            //updateArcPath()
          }
        }

        var lineWidth: CGFloat = 10 {
          didSet {
            arcLayer.lineWidth = lineWidth
          }
        }

        var startAngle: CGFloat = 0 {
          didSet {
            //updateArcPath()
          }
        }

        var endAngle: CGFloat = 0 {
          didSet {
            updateArcPath()
          }
        }
        
        var arcRadius: CGFloat = 0 {
          didSet {
            //updateArcPath()
          }
        }
        
        let arcLayer = CAShapeLayer()
        
        private var isClockWise: Bool {
            get {
                return startAngle <= endAngle ? true : false
            }
        }
        
        init() {
            // Making background transparent
            arcLayer.fillColor = UIColor.systemRed.cgColor
        }
        
        func updateBounds(_ bounds: CGRect) {
            arcLayer.bounds = bounds
            arcLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
            updateArcPath()
        }
        
        func updateArcPath() {
            let bounds = arcLayer.bounds
            let center = CGPoint(x: bounds.midX, y: bounds.midY)
            let arc = UIBezierPath(
                arcCenter: center,
                radius: arcRadius,
                startAngle: startAngle,
                endAngle: endAngle,
                clockwise: isClockWise)
            arcLayer.path = arc.cgPath
            
        }
    }
}
